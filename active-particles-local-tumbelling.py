import hoomd
import hoomd.md
import numpy as np
import os
import argparse
import tumbelling_neighbors_tools as tumble

parser = argparse.ArgumentParser()
parser.add_argument('--N-a', '--N-active', help='Number of active particles', type=int, default=1, dest='N_a')
parser.add_argument('--N-o', '--N-obstacles', help='Number of obstacles', type=int, default=0, dest='N_o')
parser.add_argument('--R-a', help='Radius of active particles', type=float, default=0.5, dest='R_a')
parser.add_argument('--R-o', help='Radius of obstacles', type=float, default=5, dest='R_o')
parser.add_argument('--obs-gap', help='Minimum gap size between obstacles', type=float, default=0, dest='obs_gap')
parser.add_argument('-L', help='Box size', type=float, default=None, dest='L')
parser.add_argument('--dim', help='Box dimensions', type=int, default=2, dest='dimensions')
parser.add_argument('--phi', help='Obstacles volume fraction', type=float, default=0.8, dest='phi')
parser.add_argument('-f', '--f-active-mag', help='Magnitude of the active force', type=float, default=1.0, dest='f_active')
parser.add_argument('-T', '--KT', help='Temperature', type=float, default=0.01, dest='T')
parser.add_argument('--dt', help='MD time-step', type=float, default=0.01, dest='dt')
parser.add_argument('--gamma', help='Viscous drag coefficient', type=float, default=1.0, dest='gamma')
parser.add_argument('--run-time', '--rt', help='Simulation final time step.', type=float, dest='run_time')
parser.add_argument('--tumble-period', '--tp', help='Tumbelling period.', type=float, dest='tumblling_period')
parser.add_argument('--dump-period', help='GSD fump period', type=float, dest='dump_period')
parser.add_argument('--dump-fname', help='GSD dump filename (without [simid].gsd)', type=str, dest='dump_fname', default='dump')
parser.add_argument('--restart-fname', help='GSD dump filename (without [simid].gsd)', type=str, dest='restart_fname', default='restart')
parser.add_argument('--simid', help='Simulation informative id', type=str, default=None, dest='simid')
parser.add_argument('--hoomd', type=str, help='Aruments to pass to HOOMD (default: cpu)', default='cpu')
args = parser.parse_args()
print(args)
N1 = args.N_a
N2 = args.N_o
N = N1 + N2
print('N=%d'%N)
if args.L==None:
    phi = args.phi
    if args.dimensions==2:
        L = (args.N_o*np.pi*(args.R_o**2)/phi)**0.5
    else:
        L = ((4./3)*args.N_o*np.pi*(args.R_o**3)/phi)**(1./3.)
else:
    L = args.L
    phi = args.N_o*np.pi*((args.R_o**2)*(args.dimensions==2) + (args.dimensions==3)*(4./3.)*args.R_o**3)/(L**args.dimensions)
print('L = {}, phi = {}'.format(L, phi))

dump_period = int(args.dump_period)
run_time = int(args.run_time)
dt = args.dt
particle_types = ['Active', 'Obstacle']
particles_parmas = {
    'Active': {'R': args.R_a},
    'Obstacle': {'R': args.R_o}
}
# dump and restart gsd files
fname_temp = '{}.gsd'
if (args.simid != None):
    fname_temp = '{}-{}.gsd'
dump_fname = fname_temp.format(args.dump_fname, args.simid)
restart_fname = fname_temp.format(args.restart_fname, args.simid)
log_fname = fname_temp[:-3].format(args.dump_fname, args.simid) + 'log'
######
callback_period = args.tumblling_period
dt = args.dt
k = 100
gamma = args.gamma
active_force_magnitude = args.f_active
######################################
# Initializing the first snapshot
######################################
hoomd.context.initialize('--mode={}'.format(args.hoomd))
if os.path.exists(restart_fname):
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True
else:
    snapshot = hoomd.data.make_snapshot(N, particle_types=particle_types, box=hoomd.data.boxdim(L=L, dimensions=args.dimensions))
    positions = np.random.rand(N, args.dimensions)*L - L*0.5
    snapshot.particles.position[:, :args.dimensions] = positions
    snapshot.particles.diameter[:] = np.array(N1*[2*args.R_a] + N2*[2*args.R_o])
    snapshot.particles.typeid[:] = np.array(N1*[0] + N2*[1])
    snapshot.particles.mass[:] = 1
    snapshot.particles.moment_inertia[:] = np.array([1, 1, 1])
    random_theta = np.random.rand(N)*2*np.pi
    if args.dimensions==3:
        u = np.random.rand(N, 3)*2 - 1
        u = u/np.linalg.norm(u, axis=1).reshape(N, 1)
    else:
        u = np.zeros(shape=(N, 3))
        u[:, -1] = 1
    snapshot.particles.orientation[:, 0] = np.cos(random_theta*0.5)
    snapshot.particles.orientation[:, 1:] = u
    snapshot.particles.orientation[:, 1:] *= np.repeat(np.sin(random_theta.reshape(N,1)*0.5), 3, axis=1)
    print(u)
    print('ssssss')
    print (snapshot.particles.orientation)
    restart = False
    system = hoomd.init.read_snapshot(snapshot)
# Groups
all = hoomd.group.all();
active_particles = hoomd.group.type('Active')
obstacle_particles = hoomd.group.type('Obstacle')

######################################
# Neighbor list and pair potentials
######################################
nl = hoomd.md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.8)
dpd_c = hoomd.md.pair.dpd_conservative(r_cut=2*args.R_o, nlist=nl)
dpd_c.pair_coeff.set('Active', 'Active', A=k, r_cut=-1*2*args.R_a)
dpd_c.pair_coeff.set('Active', 'Obstacle', A=k, r_cut=args.R_a+args.R_o)
dpd_c.pair_coeff.set('Obstacle', 'Obstacle', A=k, r_cut=2*args.R_o + args.obs_gap)

######################################
# GSD files
######################################
restart_gsd = hoomd.dump.gsd(filename=restart_fname, group=all, truncate=True, period=dump_period*100, overwrite=True, dynamic=['attribute', 'property', 'momentum', 'topology'])
dump_gsd = hoomd.dump.gsd(filename=dump_fname, group=active_particles, period=dump_period, dynamic=['attribute', 'property', 'momentum'])
dump_gsd.disable()
restart_gsd.disable()
#####################
# Removing overlaps
#####################
if not(restart):
    fire = hoomd.md.integrate.mode_minimize_fire(dt=0.001, ftol=1e-5, Etol=1e-7)
    if args.N_o > 0:
        nve = hoomd.md.integrate.nve(group=obstacle_particles)
        while not(fire.has_converged()):
            hoomd.run(1000)
        nve.disable()
    nve = hoomd.md.integrate.nve(group=active_particles)
    while not(fire.has_converged()):
        hoomd.run(1000)
    nve.disable()

obstacle_gsd = hoomd.dump.gsd(filename=dump_fname[:-4]+'-obstacles.gsd', group=obstacle_particles, overwrite=True, truncate=True, phase=-1, time_step=0, period=None)
#obstacle_gsd.disable()
#####################
# Langevin dynamics
#####################
f_lst = [(active_force_magnitude, 0, 0) for i in range(N1)]
active_force = hoomd.md.force.active(seed=1, f_lst=f_lst, group=active_particles, orientation_link=True, orientation_reverse_link=False)
hoomd.md.integrate.mode_standard(dt=dt, aniso=True)
langevin = hoomd.md.integrate.langevin(group=active_particles, kT=args.T, noiseless_r=True, noiseless_t=True, seed=4)
if args.N_o > 0:
    tumbller = tumble.Tumbller(system=system, box=system.box, r_max=6.5, local_r_cut=1.5)
    tumbller_callback = hoomd.analyze.callback(callback=tumbller.callback1, period=callback_period)
dump_gsd.enable()
restart_gsd.enable()
hoomd.run(args.run_time)
