import jax
from jax.config import config
config.update("jax_enable_x64", True)

import gsd
import gsd.hoomd
import rich
from rich.progress import track, Progress
import numpy as onp

import jax.numpy as np
from jax import random
from jax import jit
from jax import vmap
from jax import lax
from jax import grad
vectorize = np.vectorize
from functools import partial
from collections import namedtuple
import base64
from scipy.spatial.transform import Rotation
import os
import argparse
from jax_md import space, smap, energy, minimize, quantity, simulate, partition, util
from jax_md.util import f32
from jax import ops

#####################################################
@vmap
def normal(theta):
    return np.array([np.cos(theta), np.sin(theta)])

@vmap
def get_quat(theta):
    #rot = Rotation.from_rotvec(theta * np.array([1, 0, 0]))
    #q = rot.as_quat()
    rad0 = np.pi/4.
    rad1 = np.pi/4 + theta/2.
    q = np.array([np.cos(rad1)*np.cos(rad0), np.cos(rad1)*np.sin(rad0),
                  np.sin(rad1)*np.cos(rad0), np.sin(rad1)*np.cos(rad0)])
    return q
    
def gsd_append(gsd0, step, state0, i=0, j=None):
    Na = len(state0.R[i:j])

    snap = gsd.hoomd.Snapshot()
    snap.configuration.box = [box_size]*3 + [0]*3
    snap.particles.N = Na
    snap.particles.position = np.append(state0.R[i:j], np.zeros(Na).reshape(Na,1), axis=1)
    if 'theta' in state0._fields:
        n = normal(state0.theta[i:j])
        orin = np.append(n, np.zeros(2*Na).reshape(Na, 2), axis=1)
        snap.particles.velocity = orin[:, [0, 1, 2]]

        quat0 = get_quat(state0.theta[i:j])
    
        snap.particles.typeid = state0.typeid[i:j]
        snap.particles.orientation = quat0[:, [0, 1, 2, 3]]
    snap.particles.diameter = state0.D[i:j]
    gsd0.append(snap)
    
normalize = lambda v: v / np.linalg.norm(v, axis=1, keepdims=True)

#####################################################

parser = argparse.ArgumentParser()
parser.add_argument('-N', type=int, dest='N')
parser.add_argument('-l', type=float, dest='l', default=2.0)
parser.add_argument('--No', type=int, dest='No', default=0)
parser.add_argument('--Ro', type=float, dest='Ro', default=5.0)
parser.add_argument('--box-size', type=float, dest='box_size', default=None)
parser.add_argument('--phi-o', type=float, dest='phi_o', default=0.3)
parser.add_argument('--nd', type=float, dest='nd', default=0.01)
parser.add_argument('--dt', type=str, dest='dt', default='1e-2')
parser.add_argument('--rt', '--run-time', type=str, dest='run_time')
parser.add_argument('--event-rate', '--er', type=str, dest='event_rate', default='0.0')
parser.add_argument('-k', type=float, dest='k', default=50.0)
parser.add_argument('--grad-alpha', '--ga', help='scaling the normal dist of dtheta_tumble', type=str, dest='grad_alpha', default='1.0')
parser.add_argument('--lax-steps', type=int, dest='lax_steps', default=100)
parser.add_argument('--tgsd', type=int, dest='tgsd', default=100)
parser.add_argument('--fgsd', type=str, dest='fgsd', default='test-jax-brownian.gsd')
args = parser.parse_args()

Na = args.N
No = args.No
Ntot = Na + No
Ro = args.Ro
Do = 2*Ro 
dim = 2
speed = f_a = 0.1
r_buff = 1.0
r_cut = 3.0
length = args.l
lax_steps = args.lax_steps
tgsd = args.tgsd
dt = float(args.dt)
k0 = args.k
grad_alpha = float(args.grad_alpha)
event_rate = float(args.event_rate)
final_steps = int(float(args.run_time)/dt)
temperature = 1e-1
gamma = 1.0
tol = 1e-10 # zero (float)

Agents = namedtuple('agents', ['R', 'theta', 'ftot', 'ttot'])
Obstacle = namedtuple('Obstacle', ['R', 'D'])

L0 = length # np.ones(Na)*length
d0 = D0 = 1.0 # np.ones(Na)
if No>0:
    box_size = ((No*np.pi*Ro**2)/args.phi_o)**0.5
else:
    box_size = quantity.box_size_at_number_density(particle_count=Na,
                                                   number_density=args.nd,
                                                   spatial_dimension=dim)
if not (args.box_size is None):
    box_size = args.box_size
    
ogsd_fname = args.fgsd
ogsd = gsd.hoomd.open(ogsd_fname, 'wb')
obs_gsd_fname = ogsd_fname[:-4]+'-obs.gsd' #'test-jax-brownian-obs.gsd'
obs_gsd = gsd.hoomd.open(obs_gsd_fname, 'wb')

displacement, shift = space.periodic(box_size)
rng = random.PRNGKey(1)
rng, R_rng, theta_rng, Dr_rng = random.split(rng, 4)

agents = Agents(
    R = box_size * random.uniform(R_rng, (Na, dim)),
    theta = random.uniform(theta_rng, (Na,), maxval=2.*np.pi),
    ftot = np.zeros((Na, 2)),
    ttot = np.zeros(Na)
    #L = np.ones(shape=(Na,))*3,
    #d = np.ones(shape=(Na,))   
)
rng, Robs_rng = random.split(Dr_rng)
obstacles = Obstacle(
    R = box_size * random.uniform(Robs_rng, (No, 2)),
    D = np.ones(No)*2*Ro
)
Particle = namedtuple('Particle', ['R', 'theta', 'L', 'D', 'ftot', 'ttot', 'typeid'])

particles = Particle(
    R = np.vstack((agents.R, obstacles.R)),
    theta = np.hstack((agents.theta, np.zeros(No))),
    L = np.array([L0]*Na + [0.]*No),
    D = np.array([D0]*Na + [2*Ro]*No),
    ftot = np.zeros((Ntot, 2)),
    ttot = np.zeros(Ntot),
    typeid = np.array([0]*Na + [1]*No)
)
###################################################################
@jit
def dist_sc(rij_sq, rei, rej, eij):
    tol = 1.0e-6
    ell2   = L0/2.0   # Half length
    sin_sq = 1.0 - eij**2 # Squared sine of angle between line segments
    ci = np.where(sin_sq < tol, -rei, ( - rei + eij * rej ) / sin_sq)
    cj = np.where(sin_sq < tol, rej, ( rej - eij * rei ) / sin_sq)
    ai = np.fabs ( ci )
    aj = np.fabs ( cj )
    ci = np.where(ai > ell2, ell2*np.sign(ci), ci)
    cj = np.where(aj > ell2, ell2*np.sign(cj), cj)
    cj = np.where(ai > aj, rej + ci * eij, cj)
    ci = np.where(ai <= aj, -rei + cj * eij, ci)
    ai = np.fabs ( ci )
    aj = np.fabs ( cj )
    ci = np.where(ai > ell2, ell2*np.sign(ci), ci)
    cj = np.where(aj > ell2, ell2*np.sign(cj), cj)
    di =  2.0 * rei + ci - cj * eij
    dj = -2.0 * rej + cj - ci * eij
    return rij_sq + ci * di + cj * dj, ci, cj # Squared distance between line segments

@jit
def force_torque_compute(dR, N_1, N_2):
#def force_torque_compute(dR_N_2, N_1):
    #dR, N_2 = dR_N_2
    #dR = dR.reshape((2,))
    #N_2 = N_2.reshape((2,))
    dr = space.distance(dR)
    dr_sq = dr*dr
    rei = np.dot(dR, N_1)
    rej = np.dot(dR, N_2)
    eij = np.dot(N_1, N_2)
    dist2, ci, cj = dist_sc(dr_sq, rei, rej, eij)
    rij = (dR + ci*N_1 - cj*N_2)
    rij_mag = np.dot(rij, rij)**0.5#space.distance(rij)
    rij_normal = np.where(rij_mag>tol, rij/(rij_mag), 0.)
    fij = np.where(rij_mag<=d0, -k0*(d0 - rij_mag)*rij_normal, 0.)
    yij = ci*N_1 + 0.5*rij # Force arm
    tij = np.cross(yij, -fij)
    return np.hstack((fij, tij))

@jit
def ft_obstacle_compute(dR, N, D):
    Lhalf = L0*0.5
    ci = np.dot(dR, N)
    ci = np.where(np.fabs(ci) > Lhalf, Lhalf*np.sign(ci), ci)
    rij = dR - ci*N
    rij_mag = space.distance(rij)
    dij = 0.5*(D+d0)
    rij_normal = np.where(rij_mag>tol, rij/(rij_mag), 0.)
    fij = np.where(rij_mag<=dij, -k0*(dij - rij_mag)*rij_normal, 0.)
    yij = ci*N + 0.5*rij # Force arm
    tij =  np.cross(yij, fij)
    return np.hstack((fij, tij))

@jit
def compute_ft_all(dR, Ni, Nj, Dj, typeidi, typeidj):
    fn1 = force_torque_compute
    fn2 = ft_obstacle_compute
    # if i & j == agents
    ftres = np.where(typeidj == 0, fn1(dR, Ni, Nj), np.zeros(3))
    # if i==agent & j==obstacle
    ftres = np.where(np.logical_and(typeidi==0, typeidj==1), fn2(dR, Ni, Dj), ftres)
    #if i,j == obstacles
    ftres = np.where(np.logical_and(typeidi==1, typeidj==1), 0, ftres)
    return ftres[:2], ftres[-1]

@jit
def force_fn(state, neighbors):
  d = space.map_product(displacement)

  particles = state['particles']
  #FT_compute = vmap(force_torque_compute)
  FT_compute = vmap(compute_ft_all)
  # New code to extract displacement vector to neighbors and normals.
  senders, receivers = neighbors.idx
  Ri = particles.R[senders]
  Rj = particles.R[receivers]
  typeidi = particles.typeid[senders]
  typeidj = particles.typeid[receivers]
  Dj = particles.D[receivers]
  dR = -space.map_bond(displacement)(Ri, Rj)
  N = normal(particles.theta)
  Ni, Nj = N[senders], N[receivers]

  ftot_nb, ttot_nb = FT_compute(dR, Ni, Nj, Dj, typeidi, typeidj)
  ftot = particles.ftot.at[:].set(0)
  ttot = particles.ttot.at[:].set(0)
  
  ftot = ftot.at[senders].add(ftot_nb)
  #ftot = ftot.at[receivers].add(-ftot_nb)
  ttot = ttot.at[senders].add(ttot_nb)
  #ttot = ttot.at[receivers].add(-ttot_nb)
  return ftot , ttot

@jit
def chemical_field(r0, k):
    field_n = np.array([1, 0])
    res = k*np.dot(field_n, r0)
    return res

def dynamics(force_fn, dt, speed):
  @jit
  def update(_, state_and_neighbors_and_rng):
    state, neighbors, rng = state_and_neighbors_and_rng
    
    #  New code to update neighbor list.
    neighbors = neighbors.update(state['particles'].R) 

    # Particle = namedtuple('Particle', ['R', 'theta', 'L', 'D', 'ftot', 'ttot', 'typeid'])
    R, theta, L, D, ftot0, tot0, typeid = state['particles']

    ftot, ttot = force_fn(state, neighbors)
    agents_mask = typeid==0
    n = normal(state['particles'].theta) * agents_mask[:, None]
    # Tumbling
    rng, rng_event, rng_theta = random.split(rng, 3)
    selection0 = random.uniform(rng_event, (Na,))
    selection0 = np.where(selection0<event_rate, 1., 0.)
    selection = np.zeros(Ntot).at[np.arange(Na).astype(int)].set(selection0).astype(bool)
    
    # chemotaxis
    cf_grad = vmap(grad(chemical_field), (0, None))
    field_grad_n = np.where(selection[:, None], cf_grad(R, 10.), np.zeros(2))
    field_grad_theta = np.where(selection, np.arctan2(*field_grad_n.T[::-1]), 0.)
    dtheta_field = -theta + field_grad_theta
    #tumble_theta = random.uniform(rng_theta, (Ntot,), minval=0, maxval=2*np.pi)
    tumble_theta = (-random.normal(rng_theta, (Ntot, ))*grad_alpha + dtheta_field)
    tumble_theta = np.where(np.logical_and(agents_mask, selection), tumble_theta, 0)
    # Eq. motion
    R_new = shift(R, dt * (speed * n + ftot))
    theta_new = theta + (dt * ttot + tumble_theta)
    state['particles'] = Particle(R_new, theta_new, L, D, ftot, ttot, typeid)

    return state, neighbors, rng

  return update

###################################################################
update = dynamics(force_fn=force_fn, dt=dt, speed=1.)

state2 = {
    #'agents': agents,
    'obstacles': obstacles,
    #'particles': particles
}

progress = Progress(rich.progress.TextColumn("{task.description}"),
                    rich.progress.BarColumn(),
                    rich.progress.SpinnerColumn(),
                    #*Progress.get_default_columns(),
                    rich.progress.TimeElapsedColumn(),
                    rich.progress.TimeRemainingColumn())

# Remove overlaps between obstacles
energy_fn = energy.soft_sphere_pair(displacement, sigma=Do)
fire_init, fire_apply = minimize.fire_descent(energy_fn, shift)
fire_apply = jit(fire_apply)
fire_state = fire_init(obstacles.R)
E = energy_fn(fire_state.position)
step = 0
E_final = 1e-7
task0 = progress.add_task("Removing overlaps...", total=E-E_final)
with progress:
    while (E > 1e-7):
        # in track(range(200), description="Removing overlaps between obstacles..."):
        fire_state = fire_apply(fire_state)
        state2['obstacles'] = Obstacle(fire_state.position, obstacles.D)
        E = energy_fn(fire_state.position)
        DE = E-E_final
        #gsd_append(obs_gsd, step, state['obstacles'])
        step += 1
        progress.update(task0, advance=DE)

gsd_append(obs_gsd, 0, state2['obstacles'])
# Remove overlaps between agents & obstacles
# ...
# Neighbor lists
neighbor_fn = partition.neighbor_list(displacement,
                                      box_size, 
                                      r_cutoff=length+Ro, # L of agents + Ro 
                                      dr_threshold=2.1,
                                      capacity_multiplier=1.25,
                                      #cell_size=5.,
                                      format=partition.Sparse)
if No>0:
    Rnew = particles.R.at[-No:].set(state2['obstacles'].R)
else:
    Rnew = particles.R
particles = Particle(
    R = Rnew, theta = particles.theta,
    L = particles.L, D = particles.D,
    ftot = np.zeros((Ntot, 2)),
    ttot = np.zeros(Ntot),
    typeid = np.array([0]*Na + [1]*No)
)
neighbors = neighbor_fn.allocate(particles.R)
state = {
    #'agents': agents,
    #'obstacles': obstacles,
    'particles': particles
}
# Main dynamics
step = 0
progress = Progress(*progress.get_default_columns(),
                    rich.progress.SpinnerColumn(),
                    #*Progress.get_default_columns(),
                    rich.progress.TimeElapsedColumn())
task1 = progress.add_task("Running MD...", total=final_steps)
with progress:
    while step < final_steps:
        new_state, neighbors, rng = lax.fori_loop(0, lax_steps, update, (state, neighbors, rng))
        step += lax_steps
        progress.update(task1, advance=lax_steps)
        if ((step % tgsd) == 0):
            gsd_append(ogsd, step, state['particles'], 0, Na)
        # If the neighbor list can't fit in the allocation, rebuild it but bigger.
        if neighbors.did_buffer_overflow:
            #print('REBUILDING')
            neighbors = neighbor_fn.allocate(state['particles'].R)
            #print(neighbors.idx.shape)
            state, neighbors, rng = lax.fori_loop(0, lax_steps, update, (state, neighbors, rng))
            step += lax_steps
            progress.update(task1, advance=lax_steps)
            if ((step % tgsd) == 0):
                gsd_append(ogsd, step, state['particles'], 0, Na)
        else:
            state = new_state


####################################################################
