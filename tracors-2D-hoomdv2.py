#! /usr/bin/env python3

import numpy as np
import hoomd
import hoomd.md as md
import argparse
import os
import itertools

parser = argparse.ArgumentParser()
parser.add_argument('--N-t', '--N-tracors', help='Number of tracors', type=int, default=1, dest='N_t')
parser.add_argument('--N-o', '--N-obstacles', help='Number of obstacles', type=int, default=0, dest='N_o')
parser.add_argument('--R-t', help='Radius of tracors', type=float, default=0.5, dest='R_t')
parser.add_argument('--R-o', help='Radius of obstacles', type=float, default=5, dest='R_o')
parser.add_argument('--obs-gap', help='Minimum gap size between obstacles', type=float, default=0, dest='obs_gap')
parser.add_argument('-w', '--walls', help='Walls at boundaries.', type=bool, default=False, dest='walls')
parser.add_argument('-L', help='Box size', type=float, default=None, dest='L')
parser.add_argument('--dim', help='Box dimensions', type=int, default=2, dest='dimensions')
parser.add_argument('--phi', help='Obstacles volume fraction', type=float, default=0.8, dest='phi')
parser.add_argument('-T', '--KT', help='Temperature', type=float, default=1.0, dest='T')
parser.add_argument('--dt', help='MD time-step', type=float, default=0.01, dest='dt')
parser.add_argument('--gamma', help='Viscous drag coefficient', type=float, default=1.0, dest='gamma')
parser.add_argument('--run-time', '--rt', help='Simulation final time step.', type=float, dest='run_time')
parser.add_argument('--dump-period', help='GSD fump period', type=float, dest='dump_period')
parser.add_argument('--dump-fname', help='GSD dump filename (without [simid].gsd)', type=str, dest='dump_fname', default='dump')
parser.add_argument('--restart-fname', help='GSD dump filename (without [simid].gsd)', type=str, dest='restart_fname', default='restart')
parser.add_argument('--simid', help='Simulation informative id', type=str, default=None, dest='simid')
parser.add_argument('--hoomd', type=str, help='Aruments to pass to HOOMD (default: cpu)', default='cpu')
print('test')
args = parser.parse_args()
print(args)
N = args.N_t + args.N_o
print('N=%d'%N)
if args.L==None:
    phi = args.phi
    if args.dimensions==2:
        L = (args.N_o*np.pi*(args.R_o**2)/phi)**0.5
    else:
        L = ((4./3)*args.N_o*np.pi*(args.R_o**3)/phi)**(1./3.)
else:
    L = args.L
    phi = args.N_o*np.pi*((args.R_o**2)*(args.dimensions==2) + (args.dimensions==3)*(4./3.)*args.R_o**3)/(L**args.dimensions)
if args.walls:
    L = L + 2*2*args.R_t
print('L = {}, phi = {}'.format(L, phi))

dump_period = int(args.dump_period)
run_time = int(args.run_time)
dt = args.dt
particle_types = ['t', 'o']
particles_parmas = {
    't': {'R':args.R_t},
    'o': {'R':args.R_o}
}
# dump and restart gsd files
fname_temp = '{}.gsd'
if (args.simid != None):
    fname_temp = '{}-{}.gsd'
dump_fname = fname_temp.format(args.dump_fname, args.simid)
restart_fname = fname_temp.format(args.restart_fname, args.simid)
log_fname = fname_temp[:-3].format(args.dump_fname, args.simid) + 'log'
######################################
# Initializing the first snapshot
######################################
hoomd.context.initialize('--mode={}'.format(args.hoomd))
if os.path.exists(restart_fname):
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True
else:
    snap = hoomd.data.make_snapshot(args.N_t+args.N_o, particle_types=particle_types, box=hoomd.data.boxdim(L=L, dimensions=args.dimensions))
    snap.particles.position[:] = np.random.rand(snap.particles.N, 3)*(L-2*2.*args.R_t) - 0.5*(L-2*2.*args.R_t)
    if args.dimensions==2:
        snap.particles.position[:, 2] = 0 # 2D system
    snap.particles.diameter[:] = [2*args.R_t]*args.N_t + [2*args.R_o]*args.N_o
    snap.particles.typeid[:] = [0]*args.N_t + [1]*args.N_o
    snap.particles.mass[:] = 1
    restart = False
    system = hoomd.init.read_snapshot(snap)
# Groups
gr_all = hoomd.group.all()
gr_tracors = hoomd.group.type(name='gr_tracors', type='t')
gr_obstacles = hoomd.group.type(name='gr_obstacles', type='o')

######################################
# Neighbor list and pair potentials 
######################################
# Neighbor list
nl = hoomd.md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.8)
# Pair potentials
dpd = md.pair.dpd(nlist=nl, r_cut=1.0, kT=0, seed=1)
for p1, p2 in itertools.combinations_with_replacement(particle_types, 2):
    r_cut = particles_parmas[p1]['R'] + particles_parmas[p2]['R']
    dpd.pair_coeff.set(p1, p2, A=10.0, r_cut=False, gamma=args.gamma)
# Walls
if args.walls:
    walls = hoomd.md.wall.group()
    for i in range(args.dimensions):
        origin = np.zeros(3)
        normal = np.zeros(3)
        origin[i] = 0.5*L
        normal[i] = -1
        walls.add_plane(origin=origin, normal=normal)
        walls.add_plane(origin=-1*origin, normal=-1*normal)
    lj_wall = md.wall.lj(walls, r_cut=2**(1./6.))
    lj_wall.force_coeff.set('t', sigma=1, epsilon=2*args.R_t, r_cut=2**(1./6.))
    lj_wall.force_coeff.set('o', sigma=2*args.R_o, epsilon=1.0, r_cut=False)
######################################
# GSD files
######################################
restart_gsd = hoomd.dump.gsd(filename=restart_fname, group=gr_all, truncate=True, period=dump_period*100, overwrite=True, dynamic=['attribute', 'property', 'momentum', 'topology'])
dump_gsd = hoomd.dump.gsd(filename=dump_fname, group=gr_tracors, period=dump_period, dynamic=['attribute', 'property', 'momentum'])
obstacle_gsd = hoomd.dump.gsd(filename=dump_fname[:-4]+'-obstacles.gsd', group=gr_obstacles, overwrite=True, truncate=True, phase=-1, time_step=0, period=None)
dump_gsd.disable()
restart_gsd.disable()
#obstacle_gsd.disable()
######################################
# Removing overlaps
######################################
if not(restart):
    fire = md.integrate.mode_minimize_fire(dt=0.001, ftol=1e-6, Etol=1e-7)
    nve = md.integrate.nve(group=gr_obstacles)
    # First move obstacles
    dpd.pair_coeff.set('o', 'o', r_cut=2*args.R_o+args.obs_gap, gamma=args.gamma)
    while not(fire.has_converged()):
        hoomd.run(1000)
    # Second, move tracors
    dpd.pair_coeff.set('o', 'o', r_cut=False, gamma=args.gamma)
    dpd.pair_coeff.set('t', 't', r_cut=False, gamma=args.gamma)
    dpd.pair_coeff.set('t', 'o', r_cut=args.R_t+args.R_o, gamma=args.gamma)
    nve.disable()
    nve = md.integrate.nve(group=gr_tracors)
    nve.enable()
    while not(fire.has_converged()):
        hoomd.run(1000)
    obstacle_gsd.enable()
    obstacle_gsd.write_restart()
    #obstacle_gsd.disable()
    nve.disable()
######################################
# Brownian dynamics
######################################
# Standard MD integrator
integrator = md.integrate.mode_standard(dt=dt)
brownian = hoomd.md.integrate.brownian(group=gr_tracors, kT=args.T, seed=np.random.randint(1000000)+1)
brownian.set_gamma('t', gamma=args.gamma)
dpd.pair_coeff.set(particle_types, particle_types, r_cut=False, gamma=args.gamma)
dpd.pair_coeff.set('t', 'o', r_cut=args.R_t+args.R_o, gamma=args.gamma)
dump_gsd.enable()
restart_gsd.enable()
hoomd.run_upto(args.run_time)
restart_gsd.write_restart()
