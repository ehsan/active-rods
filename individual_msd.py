import numpy as np
import pandas as pd
import gsd
import gsd.hoomd
import freud
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='input gsd file')
parser.add_argument('-o', type=str, help='output filename')
parser.add_argument('-N', type=int, help='Number of particles to use. Default is 0 (using all of the particles).', default=0)
parser.add_argument('--dt', type=float, default=1)
parser.add_argument('--gsd-period', type=float, default=1, dest='gsd_period')
parser.add_argument('--t1', type=float, default=0.001)
parser.add_argument('--t2', type=float, default=0.01)
parser.add_argument('--mode', type=str, default='window')
parser.add_argument('--per-particle', '--pp', help='store MSD per particle', type=bool, default=False, dest='per_particle')
args = parser.parse_args()

# read the input file
traj = gsd.hoomd.open(args.i, 'rb')
box = freud.box.Box.from_box(traj[0].configuration.box)
N = args.N
if N==0:
    N = traj[0].particles.N
print('N: %d'%N)
# read positions
start = int(len(traj)*args.t1)
end = int(len(traj)*args.t2)
print(len(traj), start, end)
print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(args.t1, start, args.t2, end))
pos = list()
img = list()
k = 0
for t0 in traj[start:end]:
    pos_snap = t0.particles.position[:N]
    img_snap = t0.particles.image[:N]
    pos.append(pos_snap)
    img.append(img_snap)
    k += 1
    if k%10000==0:
        print(k)
pos = np.array(pos)
img = np.array(img)
# calc MSD
MSD_df = pd.DataFrame()
msd = freud.msd.MSD(box, mode=args.mode)
msd.compute(pos, img, reset=False)#True)
windows = np.arange(0, len(msd.msd)) * args.dt*args.gsd_period
MSD_df['time'] = windows
MSD_df['MSD'] = msd.msd
res_df = MSD_df
if args.per_particle:
    res_df = pd.concat([MSD_df, pd.DataFrame(msd.particle_msd)], axis=1)
res_df.set_index('time', inplace=True)
# write the output
res_df.to_csv(args.o, sep='\t', float_format='%g')
