#!/bin/bash                                                                                                                                                                                   
#SBATCH -p medium
#SBATCH -c 1
#SBATCH -t 20:1:00
#SBATCH -C scratch
#SBATCH -o logs/outfile-%J
#SBATCH -e logs/errfile-%J

export HOOMD_IMG="$$HOME/softwares/ehsan-hoomd-v2.9.6.sif"
TMP_SCRATCH=/scratch/users/$$USER/active-rods
mkdir -p $$TMP_SCRATCH/gsds

device="cpu";

for id in $$(seq $jobid1 $jobid2) ; do 

export simid="v3-${dim}D-Na$Na-No$No-Ro$Ro-phi$phi-T$T-dt$dt-t_re$t_re-tgsd$tgsd-G$Gmode-alpha$alpha-phimax$phimax-phi0$phi0-BG$BG-TM$tumble_mode-id$$id"
export dump_fname="$$TMP_SCRATCH/gsds/dump-$$simid.gsd"
export restart_fname="$$TMP_SCRATCH/gsds/restart-$$simid.gsd"
export msd_mean_fname="$$TMP_SCRATCH/msds/msd-mean-$$simid.tsv"
export msd_pp_fname="$$TMP_SCRATCH/msds/msd-pp-$$simid.tsv"

# MD simulation

run_command=( singularity exec --nv -B /home:/home -B /scratch:/scratch $$HOOMD_IMG python3 ./active-trimers-local-tumbelling-v3.py --tumble-mode $tumble_mode --N-a $Na --N-o $No --R-o $Ro --local-r-cut $lrc --dim $dim --integrator brownian --dt $dt --run-time $rt --tumble-period $t_re --dump-period $tgsd --dump-fname $$TMP_SCRATCH/gsds/dump --restart-fname $$TMP_SCRATCH/gsds/restart -T $T --simid $$simid --hoomd $device --phi $phi --G-mode $Gmode --alpha $alpha --phi-max $phimax --phi0 $phi0 --log-tumble True --BG $BG )

printf '%q ' "$${run_command[@]}";
"$${run_command[@]}";

# MSD calculation
echo $$dump_fname;
echo $$msd_mean_fname;

singularity exec -B /home:/home -B /scratch:/scratch $$HOOMD_IMG python3 ./individual_msd.py -i $$dump_fname -o $$msd_mean_fname --t1 0 --t2 1.0 -N $Na --mode direct --gsd-period $tgsd;
singularity exec -B /home:/home -B /scratch:/scratch $$HOOMD_IMG python3 ./individual_msd.py -i $$dump_fname -o $$msd_pp_fname --t1 0 --t2 1.0 -N $Na --mode direct --gsd-period $tgsd --pp True;

done
