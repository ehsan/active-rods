#!/bin/bash

STOP_PROB=$3
NSL=$4
NEW_SL=$5
# NEW_SL/Nsl = 0.2
NEW_SL_PERIOD="5e5"

#$ -l data
#$ -l h_gpu=1
#$ -cwd
#$ -V
#$ -o log_o_$$1_$$2
#$ -e log_e_$$1_$$2
#$ -m be
#$ -l m_mem_free=20G
#$ -l h_rt=40:00:00
#$ -now n

GUIX_PROFILE="/gnu/var/guix/profiles/per-user/eirani/guix-profile"
. "$GUIX_PROFILE/etc/profile"

for id in $(seq $1 $2); do singularity exec --nv -B /home:/home -B /scratch:/scratch ../ehsan-singularity-hoomdv2.8.1.simg python3 loop-extrusion-random-normal2ctcf.py -N 1000 --Nsl $NSL --sim-id id$id --dump-fname gsds/N1000-Nsl$NSL-stop_prob$STOP_PROB --run-time 6e7 --thermalize-time 5e5 --dump-period 1e4 --le-period 1e3 --new-le-N $NEW_SL --new-le-period $NEW_SL_PERIOD --remove-le-N $NEW_SL --remove-le-period $NEW_SL_PERIOD --le-stop-file N1000-stop-probs-4tads-p1-p0.txt --le-stop-prob $STOP_PROB --hoomd 'gpu' --dt 0.012 ; done


