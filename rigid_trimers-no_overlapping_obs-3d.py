import hoomd, numpy as np
#from hoomd import md
import hoomd.md as md
from optparse import OptionParser

hoomd.context.initialize()

UserArgs=hoomd.option.get_user()
print(UserArgs)
parser = OptionParser()
parser.add_option("-b", "--N_bead", type="int", help="the number of beads per polymer", default=10)
parser.add_option("-c", "--N_po", type="int", help="the number of polymers", default=20)
parser.add_option("-o", "--N_o", type="int", help="the number of obstacles", default=1)
parser.add_option("-r", "--R_bead", type="float", help="the radius of beads", default=1.0)
parser.add_option("--ro", type="float", help="the radius of obstacles", default=10.0)
parser.add_option("-l", "--L", type="float", help="System size", default=100)
parser.add_option("--mb", type="float", help="mass of each bead", default=1)
parser.add_option("--mo", type="float", help="mass of the obstacle", default=0)
parser.add_option("--gamma", type="float", help="the drag coefficient", default=1.0)
parser.add_option("--gamma_r", type="float", help="the drag coefficient for rotational motion", default=1.0)
parser.add_option("--d_r", type="float", help="rotational diffusion of the head", default=0)
parser.add_option("--F_b", type="float", help="the magnitude of the active force along contour", default=0)
parser.add_option("--K_r", type="float", help="the spring constant between any two non-bonded particles", default=100)
parser.add_option("--temp", type="float", help="temperature of the fluid,e.g. the polymer", default=1)
parser.add_option("-t", "--delta_t", type="float", help="time-discritization value", default=0.005)
parser.add_option("-T", "--t_final", type="float", help="total number of steps to run", default=50000)
parser.add_option("-m", "--dump_t", type="float", help="time-discritization for dump files", default=500)
parser.add_option("-g", "--gsd_name", help="the gsd file name")
parser.add_option("--save_init_obs", type="int", help="whether to save the initial configuration of obstacles", default=0)
(opts, args) = parser.parse_args(UserArgs)

N_bead = opts.N_bead
N_po = opts.N_po
N_o = opts.N_o
R_o = opts.ro
R_bead = opts.R_bead
L = opts.L
m_bead = opts.mb
m_o = opts.mo
Dr = opts.d_r
gamma = opts.gamma
gamma_r = opts.gamma_r
K_r = opts.K_r
temp=opts.temp
delta_t = opts.delta_t
t_final = opts.t_final
dump_period = opts.dump_t
fname = opts.gsd_name
F_b = opts.F_b
dump_fname = fname + '-N_po{}-N_ob{}-F_b{}-temp{}-gamma_r{}-L{}-dt{}-dt_gsd{}.gsd'.format(N_po,N_o,F_b,temp,gamma_r,L,delta_t,dump_period)
dump_fname0 = fname + '0-N_po{}-N_ob{}-F_b{}-temp{}-gamma_r{}-L{}-dt{}-dt_gsd{}.gsd'.format(N_po,N_o,F_b,temp,gamma_r,L,delta_t,dump_period)
log_fname = fname + '-N_po{}-N_ob{}-F_b{}-temp{}-gamma_r{}-L{}-dt{}-dt_gsd{}.log'.format(N_po,N_o,F_b,temp,gamma_r,L,delta_t,dump_period)

save_init_obs=opts.save_init_obs

I = 1
run_steps = int(t_final)

###########################################################################
# initializing the system 
###########################################################################
# make snapshot
snapshot = hoomd.data.make_snapshot(N_po+N_o, particle_types=['A','B'], box=hoomd.data.boxdim(L=L, dimensions=3))

# obstacle's coordinates
for t in range(N_o):
    snapshot.particles.position[t] = np.random.rand(3)*L - L/2.0
    snapshot.particles.diameter[t] = 2*R_o
    snapshot.particles.typeid[t] = 0

# bead's coordinates
f_lst = []
for i in range(N_po):
    t = i + N_o
    snapshot.particles.position[t] = np.random.rand(3)*L - L/2.0
    snapshot.particles.diameter[t] = 2*R_bead
    snapshot.particles.typeid[t] = 1
    snapshot.particles.mass[t] = m_bead
    snapshot.particles.moment_inertia[t] = [0,0,I]
    teta = np.random.rand()*2*np.pi
    snapshot.particles.orientation[t] = [np.cos(teta*0.5),0,0,np.sin(teta*0.5)]
    f_lst.append( tuple(F_b*np.array([1, 0, 0])))

system = hoomd.init.read_snapshot(snapshot)

system.particles.types.add('C')
rigid = md.constrain.rigid()
rigid.set_param('B', positions=[(-2*R_bead,0,0),(2*R_bead,0,0)], types=['C']*2, diameters=[2*R_bead]*2)
rigid.create_bodies()

# specify the groups
all = hoomd.group.all()
g_obstacles = hoomd.group.type(name='g_obstacles',type='A')
g_heads = hoomd.group.rigid_center() 
g_polymers = hoomd.group.difference('g_polymers', a=all, b=g_obstacles)
g_integrable = hoomd.group.union('g_integrable', a=g_heads, b=g_obstacles)

nl = md.nlist.cell(r_buff=0.3, check_period=1)

##########################################################################
# the main run:
##########################################################################
# setting hard boundary conditions
particle_types = ['A','B','C']

# removing the overlaps
dpd = md.pair.dpd(r_cut=1.0, nlist=nl, kT=0, seed=np.random.randint(1,1000000))
dpd.pair_coeff.set(particle_types, particle_types, A=1.0, r_cut=False, gamma=gamma)
dpd.pair_coeff.set('A', 'A', A=1.0, r_cut=2*R_o, gamma=gamma)
dpd.pair_coeff.set('A', 'B', A=1.0, r_cut=R_bead+R_o, gamma=gamma)
dpd.pair_coeff.set('A', 'C', A=1.0, r_cut=R_bead+R_o, gamma=gamma)
fire = md.integrate.mode_minimize_fire(dt=0.001, ftol=1e-5, Etol=1e-7)
nve = md.integrate.nve(group=g_integrable)
while not(fire.has_converged()):
        hoomd.run(1000)
dpd.disable()
nve.disable()

# interactions
dpd_c = md.pair.dpd_conservative(r_cut=-1.0, nlist=nl)
dpd_c.pair_coeff.set(particle_types, particle_types, A=K_r, r_cut=False)
dpd_c.pair_coeff.set('A', 'B', A=K_r, r_cut=R_bead+R_o)
dpd_c.pair_coeff.set('A', 'C', A=K_r, r_cut=R_bead+R_o)

# integrate at constant temperature
md.integrate.mode_standard(dt=delta_t, aniso=True)

gsd1=hoomd.dump.gsd(dump_fname, group=g_polymers, period=dump_period, overwrite=True, dynamic=['attribute','property', 'momentum', 'topology'])
gsd0=hoomd.dump.gsd(dump_fname0, group=g_obstacles, period=None, time_step=0, overwrite=True, dynamic=['attribute','property', 'momentum', 'topology'])

# run the main simulation
brow=hoomd.md.integrate.brownian(group=g_heads, seed=np.random.randint(1,1000000), kT=temp, noiseless_r=False, noiseless_t=False)
brow.set_gamma('B', gamma=gamma)
brow.set_gamma_r('B', gamma_r=gamma_r)
#act_bead=md.force.active(seed=np.random.randint(1000000), f_lst=f_lst, t_lst=[tuple(np.array([0,0,0])) for i in np.arange(N_po)], group=g_heads, rotation_diff=Dr, orientation_link=False, orientation_reverse_link=True)
act_bead=md.force.active(seed=np.random.randint(1000000), f_lst=f_lst, group=g_heads, orientation_link=True, orientation_reverse_link=False)

hoomd.run(run_steps)
