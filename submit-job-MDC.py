import os
import argparse
import tempfile

parser = argparse.ArgumentParser()
parser.add_argument('--N-t', '--N-tracors', help='Number of tracors', type=int, default=1, dest='Nt')
parser.add_argument('--N-o', '--N-obstacles', help='Number of obstacles', type=int, default=0, dest='No')
parser.add_argument('--obs-gap', help='Minimum gap size between obstacles', type=float, default=0, dest='obs_gap')
parser.add_argument('-w', '--walls', help='Walls at boundaries.', type=bool, default=False, dest='walls')
parser.add_argument('--dim', help='Box dimensions', type=int, default=2, dest='dimensions')
parser.add_argument('--phi', help='Obstacles volume fraction', type=float, default=0.8, dest='phi')
parser.add_argument('-T', '--KT', help='Temperature', type=float, default=1.0, dest='T')
parser.add_argument('--dt', help='MD time-step', type=float, default=0.01, dest='dt')
parser.add_argument('--run-time', '--rt', help='Simulation final time step.', type=float, dest='run_time', default=3e7)
parser.add_argument('--dump-period', help='GSD fump period', type=float, dest='dump_period', default=1e4)
parser.add_argument('--simid', help='Simulation informative id', type=str, default=None, dest='simid')
parser.add_argument('--submit', type=bool, default=False)
args = parser.parse_args()
tf = tempfile.NamedTemporaryFile(delete=False, dir='./', suffix='.job')
params = {
    'id1': 1,
    'id2': 5,
    'Nt': args.Nt,
    'No': args.No,
    'T': args.T,
    'dt': args.dt,
    'phi': args.phi,
    'dim': args.dimensions,
    'run_time': args.run_time,
    'dump_period': args.dump_period,
    'walls': '-w True'*args.walls,
    'walls2': 'walls-'*args.walls
}
with open('job-run-sims-MDC-GPU-cluster.template', 'r') as template_f:
    t = template_f.read()
t = t.format(**params)
tf.write(t.encode('utf-8'))
print(tf.name)
if args.submit:
    os.system('qsub %s'%tf.name)
    os.system('sleep 1')
tf.close()
