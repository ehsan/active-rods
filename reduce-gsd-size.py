import numpy as np
import gsd
import gsd.hoomd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='input gsd file')
parser.add_argument('-o', type=str, help='output gsd file')
args = parser.parse_args()

igsd = gsd.hoomd.open(args.i, 'rb')
ogsd = gsd.hoomd.open(args.o, 'wb')
snap0 = igsd[0]
N0 = snap0.particles.N
active_ids = snap0.particles.typeid == 0
N = np.sum(active_ids)

for snap0 in igsd:
    active_ids = snap0.particles.typeid == 0
    snap = gsd.hoomd.Snapshot()
    snap.configuration = snap0.configuration
    snap.particles.N = N
    snap.particles.position = snap0.particles.position[active_ids]
    snap.particles.image = snap0.particles.image[active_ids]
    snap.particles.orientation = snap0.particles.orientation[active_ids]
    ogsd.append(snap)
igsd.close()
ogsd.close()
