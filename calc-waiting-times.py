import numpy as np
import pandas as pd
import gsd
import gsd.hoomd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='input gsd file')
parser.add_argument('-o', type=str, help='output filename')
parser.add_argument('--gsd-period', type=float, default=1, dest='gsd_period', help='gsd dumping period (default: 1)')
parser.add_argument('--t1-frac', type=float, default=None, help='fractional starting time (between 0 to 1)', dest='t1_frac')
parser.add_argument('--t2-frac', type=float, default=None, help='fractional ending time (between 0 to 1)', dest='t2_frac')
parser.add_argument('--t1', type=int, default=0)
parser.add_argument('--t2', type=int, default=-1)
parser.add_argument('--distance-thr', help='Trapping distance threshold', type=float, default=5.0, dest='dist_thr')
args = parser.parse_args()
traj = gsd.hoomd.open(args.i, 'rb')
s = traj[0]
if (args.t1_frac is None):
    start = args.t1
    if start < 0:
        start = len(traj) + start
else:
    start = int(len(traj)*args.t1_frac)

if (args.t2_frac is None):
    end = args.t2
    if end < 0:
        end = len(traj) + end
else:
    end = int(len(traj)*args.t2_frac)
dist_thr = args.dist_thr
##########################################
# read the input file
p_ids = s.particles.typeid == 0
L = s.configuration.box[0]
# read positions
print(len(traj), start, end)
print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(start, start, end, end))
df_rows = dict()
timesteps = []
for s0 in traj[start:end]:
    timesteps.append(s0.configuration.step)
    pos = s0.particles.position[p_ids]
    img = s0.particles.image[p_ids]
    for i, p0 in enumerate(pos):
        k = 'p%d'%i
        if not(k+'-x' in df_rows.keys()):
            df_rows[k+'-x'] = []
            df_rows[k+'-y'] = []
        df_rows[k+'-x'].append(pos[i, 0] + L*img[i, 0])
        df_rows[k+'-y'].append(pos[i, 1] + L*img[i, 1])
df = pd.DataFrame(df_rows, index=timesteps)
# %%
pool = []
for p0_x, p0_y in zip(df.columns[::2], df.columns[1::2]):
    d = (np.diff(df[p0_x])**2 + np.diff(df[p0_y])**2)**0.5
    trapped_x = np.zeros(len(d)+1)
    trapped_x[1:][d < dist_thr] = 1
    steps = np.arange(len(trapped_x))+1
    d_trapped_x = np.diff(trapped_x)
    start_trapped_t = steps[1:][d_trapped_x == 1]
    end_trapped_t = steps[1:][d_trapped_x == -1]
    waiting_times_len = min(len(end_trapped_t), len(start_trapped_t))
    waiting_times = end_trapped_t[:waiting_times_len] - start_trapped_t[:waiting_times_len]
    pool = pool + waiting_times.tolist()
pool = np.array(pool)*args.gsd_period
header_str = 'N_particles: {N}, N_snapshots: {N_snapshots}, gsd_period:{gsd_period}'.format(N=np.sum(p_ids), N_snapshots=end-start, gsd_period=args.gsd_period)
np.savetxt(args.o, pool, fmt='%.5g', header=header_str)
with open(args.o+'.npy', 'wb') as fout:
    np.save(fout, pool)
