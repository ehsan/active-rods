import numpy as np
import random

N = 100      # number of dimers
N_frac = 10
t_i = 0
t_f = 50000
step = 10
t_steps = np.arange(t_i, t_f, step)
t_tum = []     # list for all the tumbling durations
flag = np.zeros((N), dtype=bool)
t0 = np.zeros(N)

for t in t_steps:
    lst = random.sample(range(N),N_frac)
    for i in lst:
        if flag[i]:     # set the final time
            t_tum_i = t - t0[i]
            flag[i] = False
            t_tum.append(t_tum_i)
        else:      # set the initial time   
            t0[i] = t
            flag[i] = True

t_tum = np.array(t_tum)
my_bins = np.arange(min(t_tum), max(t_tum), step)
hist, bin_edges = np.histogram(t_tum, bins=my_bins, density=True)
x_plot = 0.5 * (bin_edges[:-1]+bin_edges[1:]) 
plot(x_plot,hist)
#plt.hist(t_tum, bins=my_bins)

yscale('log')
