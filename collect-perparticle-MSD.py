import numpy as np
import pandas as pd
import glob, os, re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--i-flist', type=str, dest='iflist')
parser.add_argument('-o', type=str)
parser.add_argument('--phi', type=str)
parser.add_argument('--Pe', type=str)
parser.add_argument('--t-re', type=str, dest='t_re')
parser.add_argument('--dt', type=str, dest='dt')
parser.add_argument('--tgsd', type=str, dest='tgsd')
args = parser.parse_args()

phi = args.phi
Pe = args.Pe
t_re = args.t_re
dt = args.dt
tgsd = args.tgsd
ofname = args.o

with open(args.iflist) as iflist:
    flist = iflist.readlines()

df = pd.DataFrame()
re_pattern = r'(?<=-id).*(?=.gsd)'
flist.sort(key=lambda x: re.findall(re_pattern, x)[0])
for f0 in flist:
    idx = re.findall(re_pattern, f0)
    if len(idx) == 0:
        continue
    idx = idx[0]
    df0 = pd.read_csv(f0, delim_whitespace=True, index_col=0)
    df0.to_hdf(ofname, key=idx, mode='a', complevel=9)
    #if len(df) == 0:
    #    df['index'] = df0.index
    #    df.set_index('index', inplace=True)
    #for c0 in df0.columns:
    #    if ('MSD' in c0):
    #        continue
    #    df[idx+'-'+c0] = df0[c0]
#msd_cols = [c0 for c0 in df.columns if 'MSD' in c0]
#print(msd_cols)
#df['MSD'] = np.nanmean(df.values, axis=1)
#df.to_csv(tofname.format(phi=phi, Pe=Pe, t_re=t_re, dt=dt, tgsd=tgsd), sep='\t', float_format='%g')
