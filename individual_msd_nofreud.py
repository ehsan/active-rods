import numpy as np
import pandas as pd
import gsd
import gsd.hoomd
import freud
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='input gsd file')
parser.add_argument('-o', type=str, help='output filename')
parser.add_argument('-N', type=int, help='Number of particles to use. Default is 0 (using all of the particles).', default=0)
parser.add_argument('--dt', type=float, default=1)
parser.add_argument('--gsd-period', type=float, default=1, dest='gsd_period')
parser.add_argument('--t1', type=float, default=0.001)
parser.add_argument('--t2', type=float, default=0.01)
parser.add_argument('--step', type=float, default=0.)
parser.add_argument('--mode', type=str, default='window')
parser.add_argument('--per-particle', '--pp', help='store MSD per particle', type=bool, default=False, dest='per_particle')
args = parser.parse_args()

# read the input file
traj = gsd.hoomd.open(args.i, 'rb')
box = freud.box.Box.from_box(traj[0].configuration.box)
N = args.N
if N==0:
    N = traj[0].particles.N
print('N: %d'%N)
# read positions
start = int(len(traj)*args.t1)
end = int(len(traj)*args.t2)
if args.step > 0:
    step = int(len(traj)*args.step)
else:
    step = 1
print(len(traj), start, end)
print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(args.t1, start, args.t2, end))
pos = list()
img = list()
k = 0
for t0 in traj[start:end:step]:
    pos_snap = t0.particles.position[:N]
    img_snap = t0.particles.image[:N]
    pos.append(pos_snap)
    img.append(img_snap)
    k += 1
    if k%10000==0:
        print(k)
pos = np.array(pos)
img = np.array(img)
# calc MSD
MSD_df = pd.DataFrame()
unwrapped_pos = pos + box.L * img
dr = unwrapped_pos - unwrapped_pos[0]
dr2 = np.power(dr, 2)
dr2_mag = np.sum(dr2, axis=2)
dr2_mean = np.mean(dr2_mag, axis=1)
#msd = freud.msd.MSD(box, mode=args.mode)
#msd.compute(pos, img, reset=False)#True)
windows = np.arange(start, end, step) * args.dt*args.gsd_period
MSD_df['time'] = windows
MSD_df['MSD'] = dr2_mean
if args.per_particle:
    pp_df = pd.DataFrame(index=MSD_df.index, columns = ['p%d'%i for i in range(dr2_mag.shape[1])], data=dr2_mag[:])
    res_df = pd.concat([MSD_df, pp_df], axis=1)
else:
    res_df = MSD_df
    #for i in range(dr2_mag.shape[1]):
    #    res_df['p%d'%i] = dr2_mag[:, i]
        
res_df.set_index('time', inplace=True)
# write the output
res_df.to_csv(args.o, sep='\t', float_format='%g')
