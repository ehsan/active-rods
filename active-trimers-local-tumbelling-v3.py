import hoomd
import hoomd.md
import numpy as np
import pandas as pd
from numpy.random import default_rng
import os
import argparse
import tumbelling_neighbors_tools as tumble

parser = argparse.ArgumentParser()
parser.add_argument('--N-a', '--N-active',
                    help='Number of active particles', type=int, default=1, dest='N_a')
parser.add_argument('--agent-type', help='Agent type: trimer (default) or disc', type=str, default='trimer', dest='agent_type')
parser.add_argument('--N-o', '--N-obstacles',
                    help='Number of obstacles', type=int, default=0, dest='N_o')
parser.add_argument('--R-a', help='Radius of active particles',
                    type=float, default=0.5, dest='R_a')
parser.add_argument('--R-o', help='Radius of obstacles',
                    type=float, default=5, dest='R_o')
parser.add_argument('--N-ov1', help='Number of void obstacles1',
                    type=int, default=0, dest='N_ov1')
parser.add_argument('--R-ov1', help='Radius of void obstacles1',
                    type=float, default=0, dest='R_ov1')
parser.add_argument('--N-ov2', help='Number of void obstacles2',
                    type=int, default=0, dest='N_ov2')
parser.add_argument('--R-ov2', help='Radius of void obstacles2',
                    type=float, default=0, dest='R_ov2')
parser.add_argument('--obs-dist', help='Initial dist. of obstacles (uniform or norm)',
                    type=str, default='uniform', dest='obs_dist')
parser.add_argument('--obs-dist-sigma', help='Scale for the obstacles distribution',
                    type=float, default=1, dest='obs_dist_sigma')
parser.add_argument('--local-r-cut', help='local_r_cut',
                    type=float, default=5, dest='local_r_cut')
parser.add_argument('--obs-gap', help='Minimum gap size between obstacles',
                    type=float, default=0, dest='obs_gap')
parser.add_argument('-L', help='Box size', type=float, default=None, dest='L')
parser.add_argument('--dim', help='Box dimensions',
                    type=int, default=2, dest='dimensions')
parser.add_argument('--phi', help='Obstacles volume fraction',
                    type=float, default=0.8, dest='phi')
parser.add_argument('--v-act', '--f-act', help='Magnitude of the active velocity (force)',
                    type=float, default=1.0, dest='v_act')
parser.add_argument('-T', '--KT', help='Temperature',
                    type=float, default=1.0, dest='T')
parser.add_argument('--integrator', help='Browinan (default) or Langevin',
                    type=str, default='brownian', dest='integrator')
parser.add_argument('--dt', help='MD time-step',
                    type=float, default=0.01, dest='dt')
parser.add_argument('--gamma', help='Viscous drag coefficient',
                    type=float, default=1.0, dest='gamma')
parser.add_argument('--run-time', '--rt',
                    help='Simulation final time in the unit of t0.', type=float, dest='run_time', default=1e4)
parser.add_argument('--tumble-period', '--tp',
                    help='Tumbelling period.', type=float, dest='tumblling_period')
parser.add_argument('--dump-period', help='GSD dump period in the unit of t0',
                    type=float, dest='dump_period')
parser.add_argument('--restart-period', help='GSD restart period in the unit of t0',
                    type=float, dest='restart_period', default=None)
parser.add_argument('--tumble-mode', '--tm', help='tumble_mode, uniform or reverse',
                    type=str, default='uniform', dest='tumble_mode')
parser.add_argument('--G-mode', help='local-phi mapping function (linear or exp or sigmoid or norm or uniform)',
                    type=str, default='linear', dest='G_mode')
parser.add_argument('--alpha', help='G_mode exp: alpha (should be larger than 1 for exp), scale for norm.',
                    type=float, default=2.0, dest='alpha')
parser.add_argument('--phi-max', help='G_mode exp: phi-max, default=0.9069',
                    type=float, default=0.9069, dest='phi_max')
parser.add_argument('--phi0', help='G_mode norm: phi0 is the position of the peak, default=0.3',
                    type=float, default=0.3, dest='phi0')
parser.add_argument('--BG', help='G_mode norm: BackGround prob. for reorientation. The norm func is added to it.',
                    type=float, default=0.3, dest='BG')
parser.add_argument('--seed', help='RNG seed',
                    type=float, default=None, dest='seed')
parser.add_argument('--log-tumble', help='Log tumbleing events?',
                    type=bool, default=False, dest='log_tumble')
parser.add_argument(
    '--dump-fname', help='GSD dump filename (without [simid].gsd)', type=str, dest='dump_fname', default='dump')
parser.add_argument(
    '--restart-fname', help='GSD dump filename (without [simid].gsd)', type=str, dest='restart_fname', default='restart.restart')
parser.add_argument('--simid', help='Simulation informative id',
                    type=str, default=None, dest='simid')
parser.add_argument('--hoomd', type=str,
                    help='Aruments to pass to HOOMD (default: cpu)', default='cpu')
args = parser.parse_args()
print(args)
N1 = args.N_a
N2 = args.N_o * (args.phi > 0.0)
N = N1 + N2
R_a = args.R_a
print('N=%d' % N)
if args.L is None:
    phi = args.phi
    if args.dimensions == 2:
        L = (args.N_o*np.pi*(args.R_o**2)/phi)**0.5
    else:
        L = ((4./3)*args.N_o*np.pi*(args.R_o**3)/phi)**(1./3.)
else:
    L = args.L
    phi = args.N_o*np.pi*((args.R_o**2)*(args.dimensions == 2) +
                          (args.dimensions == 3)*(4./3.)*args.R_o**3)/(L**args.dimensions)
print('L = {}, phi = {}'.format(L, phi))
# Void obstacles

#if (args.R_ov1 > 0.):
N_ov1 = args.N_ov1
N_ov2 = args.N_ov2
N_ov = N_ov1 + N_ov2
print ('Void obstacles, N and R:', N_ov1, args.R_ov1, N_ov2, args.R_ov2)
N = N + N_ov
print('N', N)
#

dump_period = int(args.dump_period/args.dt)
print('dump_period', args.dump_period, args.dt, args.dump_period/args.dt)
run_time = int(args.run_time/args.dt)

particle_types = ['Active', 'Obstacle', 'Void obstacle1', 'Void obstacle2', 'Head', 'Tail']
particles_parmas = {
    'Active': {'R': args.R_a},
    'Obstacle': {'R': args.R_o},
    'Void obstacle1': {'R': args.R_ov1},
    'Void obstacle2': {'R': args.R_ov2}
}
# dump and restart gsd files
fname_temp = '{}.gsd'
if (args.simid != None):
    fname_temp = '{}-{}.gsd'
dump_fname = fname_temp.format(args.dump_fname, args.simid)
restart_fname = fname_temp.format(args.restart_fname, args.simid)
log_fname = fname_temp[:-3].format(args.dump_fname, args.simid) + 'log'
######
gamma = args.gamma
KT = args.T
# Unit of time
#t0 = gamma/KT
# Pe = (f_act * t_re) / R_t
# f_act = (Pe * t_re) / R_t
active_force_magnitude = args.v_act
Pe = (active_force_magnitude * args.tumblling_period) / (2*R_a)
t0 = 2*R_a / active_force_magnitude
dt = args.dt*t0
if args.tumblling_period > 0:
    callback_period = int(args.tumblling_period/args.dt)
else:
    callback_period = -1  # deactivated
    
k0 = 10000*KT
k = 100#min(100, Pe)
#k0 = 250
#k = 250

######################################
# Initializing the first snapshot
######################################
rng = default_rng(args.seed)
hoomd.context.initialize('--mode={}'.format(args.hoomd))
if os.path.exists(restart_fname):
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True
else:
    snapshot = hoomd.data.make_snapshot(
        N, particle_types=particle_types, box=hoomd.data.boxdim(L=L, dimensions=args.dimensions))
    positions = rng.random((N, args.dimensions))*L - L*0.5
    if args.obs_dist!='uniform':
        pos_obstacles = rng.normal(0, args.obs_dist_sigma, (N2+N_ov, args.dimensions))
        pos_obstacles[pos_obstacles>1] = pos_obstacles[pos_obstacles>1]  - 1.0
        pos_obstacles[pos_obstacles<-1] = pos_obstacles[pos_obstacles<-1]  + 1.0
        pos_obstacles = (pos_obstacles - pos_obstacles.min())/(pos_obstacles.max()-pos_obstacles.min())
        positions[-N2-N_ov:, 0] = pos_obstacles[:, 0]*L - L*0.5
        
    snapshot.particles.position[:, :args.dimensions] = positions
    snapshot.particles.diameter[:] = np.array(
        N1*[2*args.R_a] + N2*[2*args.R_o] + N_ov1*[2*args.R_ov1] + N_ov2*[2*args.R_ov2])
    snapshot.particles.typeid[:] = np.array(N1*[0] + N2*[1] + N_ov1*[2] + N_ov2*[3])
    snapshot.particles.mass[:] = 1
    snapshot.particles.moment_inertia[:] = np.array(
        [0, args.dimensions == 3, 1]).astype(float)
    random_theta = rng.random(N)*2*np.pi
    if args.dimensions == 3:
        u = rng.random((N, 3))*2 - 1
        u = u/np.linalg.norm(u, axis=1).reshape(N, 1)
    else:
        u = np.zeros(shape=(N, 3))
        u[:, -1] = 1
    snapshot.particles.orientation[:, 0] = np.cos(random_theta*0.5)
    snapshot.particles.orientation[:, 1:] = u
    snapshot.particles.orientation[:, 1:] *= np.repeat(
        np.sin(random_theta.reshape(N, 1)*0.5), 3, axis=1)
    print(u)
    restart = False
    system = hoomd.init.read_snapshot(snapshot)
# system.particles.types.add('C')
if ('trimer' in args.agent_type):
    rigid = hoomd.md.constrain.rigid()
    rigid.set_param('Active', positions=[
                    (2*args.R_a, 0, 0), (-2*args.R_a, 0, 0)], types=['Head', 'Tail'], diameters=[2*args.R_a]*2)
    rigid.create_bodies()
# Groups
all = hoomd.group.all()
active_particles = hoomd.group.type('Active')
head_particles = hoomd.group.type('Head')
tail_particles = hoomd.group.type('Tail')
patch_particles = hoomd.group.union(
    'g_patch', a=head_particles, b=tail_particles)
dump_particles = hoomd.group.union(
    'g_dump', a=active_particles, b=patch_particles)
obstacle_particles = hoomd.group.type('Obstacle')
voidobstacle1_particles = hoomd.group.type('Void obstacle1')
voidobstacle2_particles = hoomd.group.type('Void obstacle2')
voidobs_particles = hoomd.group.union('g_voidobs', a=voidobstacle1_particles, b=voidobstacle2_particles)
allobs_particles = hoomd.group.union('g_obs', a=obstacle_particles, b=voidobs_particles)
integrable_particles = hoomd.group.union(
    'g_integrables', a=active_particles, b=patch_particles)

######################################
# Neighbor list and pair potentials
######################################
nl = hoomd.md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.8)
dpd_c = hoomd.md.pair.dpd_conservative(r_cut=2*args.R_o, nlist=nl)
dpd_c.pair_coeff.set('Active', 'Active', A=k, r_cut=-1*2*args.R_a)
dpd_c.pair_coeff.set('Active', ['Head', 'Tail'], A=k, r_cut=-1*2*args.R_a)
dpd_c.pair_coeff.set(['Head', 'Tail'], ['Head', 'Tail'],
                     A=k, r_cut=-1*2*args.R_a)
dpd_c.pair_coeff.set('Active', 'Obstacle', A=k, r_cut=args.R_a+args.R_o)
dpd_c.pair_coeff.set('Obstacle', ['Head', 'Tail'],
                     A=k, r_cut=args.R_a+args.R_o)
dpd_c.pair_coeff.set('Obstacle', 'Obstacle', A=k,
                     r_cut=2*args.R_o + args.obs_gap)
dpd_c.pair_coeff.set(['Void obstacle1', 'Void obstacle2'], particle_types, A=k, r_cut=False)
dpd_c.pair_coeff.set('Void obstacle1', 'Void obstacle1', A=k**0.5, r_cut=2*args.R_ov1)
dpd_c.pair_coeff.set('Void obstacle2', 'Void obstacle2', A=k**0.5, r_cut=2*args.R_ov2)
dpd_c.pair_coeff.set('Void obstacle1', 'Void obstacle2', A=k**0.5, r_cut=args.R_ov1 + args.R_ov2)
dpd_c.pair_coeff.set('Void obstacle1', 'Obstacle', A=k**0.5, r_cut=args.R_ov1+args.R_o)
dpd_c.pair_coeff.set('Void obstacle2', 'Obstacle', A=k**0.5, r_cut=args.R_ov2+args.R_o)

######################################
# GSD files
######################################
if (args.restart_period==None):
    restart_period = int(dump_period * 100)
else:
    restart_period = int(args.restart_period)
print(dump_period, restart_period)
restart_gsd = hoomd.dump.gsd(filename=restart_fname, group=all, truncate=True, period=restart_period, overwrite=True, dynamic=['attribute', 'property', 'momentum', 'topology'])
#dump_gsd = hoomd.dump.gsd(filename=dump_fname, group=all, period=dump_period, dynamic=[
dump_gsd = hoomd.dump.gsd(filename=dump_fname, group=dump_particles, period=dump_period, dynamic=[
                          'attribute', 'property', 'momentum', 'topology'])
print(restart_period)
dump_gsd.disable()
restart_gsd.disable()
#####################
# Removing overlaps
#####################
obstacle_gsd0 = hoomd.dump.gsd(filename=dump_fname[:-4]+'-obstacles0.gsd', group=allobs_particles,
                              overwrite=True, truncate=False, phase=-1, period=dump_period)
obstacle_gsd0.disable()
if not(restart):
    fire = hoomd.md.integrate.mode_minimize_fire(
        dt=0.001, ftol=1e-5, Etol=1e-7)
    if N2 > 0:
        nve = hoomd.md.integrate.nve(group=allobs_particles)
        while not(fire.has_converged()):
            hoomd.run(1000)
        nve.disable()
    nve = hoomd.md.integrate.nve(group=active_particles)
    while not(fire.has_converged()):
        hoomd.run(1000)
    nve.disable()

obstacle_gsd = hoomd.dump.gsd(filename=dump_fname[:-4]+'-obstacles.gsd', group=allobs_particles,
                              overwrite=True, truncate=True, phase=-1, time_step=0, period=None)
obstacle_gsd0.disable()
#dpd_c.pair_coeff.set(particle_types, particle_types, A=k0)
#####################
# Langevin dynamics
#####################
dpd_c.pair_coeff.set('Obstacle', 'Obstacle', A=k,
                     r_cut=False)
f_lst = [(active_force_magnitude, 0, 0) for i in range(N1)]
active_force = hoomd.md.force.active(
    seed=rng.integers(1000000)+1, f_lst=f_lst, group=active_particles, orientation_link=True, orientation_reverse_link=False)
hoomd.md.integrate.mode_standard(dt=dt, aniso=True)
if args.integrator.lower() == 'brownian':
    integrator = hoomd.md.integrate.brownian(
        group=active_particles, kT=KT, noiseless_r=not(KT>0), noiseless_t=True, seed=rng.integers(1000000)+1)
if args.integrator.lower() == 'langevin':
    integrator = hoomd.md.integrate.langevin(group=active_particles, kT=KT, noiseless_r=not(KT>0), noiseless_t=True, seed=rng.integers(10000000)+1)
for p in particle_types:
    #integrator.set_gamma(p, gamma=args.gamma)
    integrator.set_gamma_r(p, gamma_r=args.gamma)
print(integrator)
#if (args.N_o > 0) and (float(args.phi) > 0.):
#if (args.N_o > 0) and (float(args.phi) > 0.):
log_dict = {}
if args.log_tumble:
    for i in range(N1):
        log_dict[i] = [0]
        tumbller = tumble.Tumbller(system=system, box=system.box, r_max=args.R_o+args.local_r_cut, local_r_cut=args.local_r_cut,agent_type=args.agent_type, G_mode=args.G_mode, alpha=args.alpha, phi_max=args.phi_max, phi0=args.phi0, BG=args.BG, log=args.log_tumble, log_dict=log_dict, tumble_mode=args.tumble_mode, seed=rng.integers(10000000)+1)
if callback_period>0:
    tumbller_callback = hoomd.analyze.callback(callback=tumbller.callback1, period=callback_period)

    
dump_gsd.enable()
restart_gsd.enable()
hoomd.run_upto(run_time)
restart_gsd.write_restart()
if args.log_tumble:
    max_n_tumble = max([len(v) for v in log_dict.values()])
    log_df = pd.DataFrame(index=range(max_n_tumble), columns=['p{}'.format(k) for k in log_dict.keys()])
    for k in log_dict.keys():
        vals = log_dict[k]
        #print(k, np.count_nonzero(~np.isnan(log_dict[k])), len(vals))
        log_df.iloc[:len(vals), k] = np.array(vals)
    log_df.to_csv(dump_fname[:-4]+'log-tumble.tsv', sep='\t', header=True, na_rep='nan')
