import numpy as np
import pandas as pd
import gsd
import gsd.hoomd
import argparse
from scipy.spatial import distance
from shapely.geometry import LineString
from shapely.geometry import Point
from shapely.ops import unary_union
from progress.bar import Bar
from progress.bar import Bar

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='input gsd file')
parser.add_argument('-o', type=str, help='output filename')
parser.add_argument(
    '--Lhalf',
    type=float,
    default=-1,
    dest='Lhalf',
    help='Length/2 of the sampling box (default: -1, the whole box)')
parser.add_argument('--x0',
                    type=float,
                    default=0,
                    help='x0 of the sampling box',
                    dest='x0')
parser.add_argument('--y0',
                    type=float,
                    default=0,
                    help='y0 of the sampling box',
                    dest='y0')
parser.add_argument('--pid',
                    type=int,
                    default=0,
                    help='particle typeid of obstacles',
                    dest='pid')
parser.add_argument('--pR',
                    type=float,
                    default=5.0,
                    help='particle'
                    's radius (default: 5.0)',
                    dest='pR')
parser.add_argument('--Nrp',
                    help='Number of random points (default: 100)',
                    type=int,
                    default=100,
                    dest='Nrp')
parser.add_argument('--l-lim',
                    help='limit on the line length (default: 200)',
                    type=float,
                    default=200,
                    dest='l_lim')
parser.add_argument('--seed',
                    help='rng seed (default None)',
                    type=int,
                    default=None,
                    dest='seed')
parser.add_argument('--rand-box',
                    help='Choose random x0,y0',
                    type=bool,
                    default=False,
                    dest='rand_box')
args = parser.parse_args()
rng = np.random.default_rng(args.seed)
traj = gsd.hoomd.open(args.i, 'rb')
s = traj[0]
##########################################
# read the input file
p_ids = s.particles.typeid == args.pid
L0 = s.configuration.box[0]
L = args.Lhalf
if L < 0:
    L = L0
x0 = args.x0
y0 = args.y0
if args.rand_box:
    x0 = y0 = rng.random() * (0.5*L0-L) - (0.5*(0.5*L0-L))
print('x0, y0:', x0, y0)
# read positions
pos0 = s.particles.position[p_ids]
#pos0 = s.particles.position
pos = pos0[np.abs(pos0[:, 0] - x0) < L]
pos = pos[np.abs(pos[:, 1] - y0) < L]
#circles = [Point(*p).buffer(args.pR) for p in pos[:, :2]]
circles_boundary = [Point(*p).buffer(args.pR).boundary for p in pos[:, :2]]
#circles_boundary = [c.boundary for c in circles]

print('#N obstacles:', len(circles_boundary))
#for c in circles_boundary:
#    plt.plot(*c.xy, color='black', lw=1)
#merged_circles = unary_union(circles)
merged_circles_boundary = unary_union(circles_boundary)

# Random lines
N = args.Nrp
ij = np.triu_indices(N, k=1)
print('Number of initial random lines:', len(ij[0]))
points = rng.random((N, 2)) * 2 * L - L + np.array([[x0, y0]])
bar = Bar('Cleaning random points', max=len(points), suffix='%(percent)d%%')
for i, p0 in enumerate(points):
    #within_flag = Point(*p0).within(merged_circles)
    dist = distance.cdist(p0[None, :], pos[:, :2])
    min_dist = dist.min(axis=1)
    within_flag = min_dist < args.pR
    #while(Point(*points[i]).distance(merged_centers)<5.0):
    while (within_flag):
        points[i][:] = rng.random(2) * 2 * L - L + np.array([[x0, y0]])
        dist = distance.cdist(p0[None, :], pos[:, :2])
        min_dist = dist.min()
        within_flag = min_dist < args.pR
        #within_flag = Point(*points[i]).within(merged_circles)
    bar.next()
bar.finish()

lines = []
for p1, p2 in zip(points[ij[0]], points[ij[1]]):
    if (np.abs(p1[0] - p2[0]) > args.l_lim) or (
        (np.abs(p1[1] - p2[1]) > args.l_lim)):
        continue
    lines.append(LineString([p1, p2]))
print('Number of lines after length filter', len(lines))
#chords = []
chords_length = []
bar = Bar('Cleaning lines', max=len(lines), suffix='%(percent)d%%')
for i, l in enumerate(lines):
    if (l.intersects(merged_circles_boundary) == False):
        #chords.append(l)
        chords_length.append(l.length)
    bar.next()
bar.finish()
print('Number of lines without intersection with particles',
      len(chords_length))
# %%
header_str = 'L: {L}, x0,y0: {x0}, {y0}, \nNparticles: {N}, Nrp: {Nrp}, Nchords:{Nchords}'.format(
    N=len(pos),
    Nrp=len(ij[0]),
    Nchords=len(chords_length),
    L=args.Lhalf,
    x0=x0,
    y0=y0)
np.savetxt(args.o, np.array(chords_length), fmt='%.5g', header=header_str)
#with open(args.o + '.npy', 'wb') as fout:
#    np.save(fout, np.array(chords_length))
