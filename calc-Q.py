import numpy as np
import pandas as pd
import h5py
import glob, os, re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='a msd-perparticle*-allsims-*.hdf file')
parser.add_argument('-o', type=str)
args = parser.parse_args()

# list of simulation keys ind the hdf file
hdf_store = h5py.File(args.i, 'r')
sim_keys = list(hdf_store.keys())
hdf_store.close()

d_range = np.arange(5, 250, 5)
weight = 1./len(sim_keys)
for sim_key in sim_keys:
    df0 = pd.read_hdf(args.i, sim_key)
    Q0 = pd.DataFrame(index=df0.index, columns=[d for d in range(5, 250, 5)])
    if sim_key == sim_keys[0]:
        Q_net = Q0.copy(deep=True)
    for d in d_range:
        particle_cols = [c0 for c0 in df0.columns if not('MSD' in c0)]
        df0 = df0[particle_cols]
        d2 = d*d
        Q0[d] = np.nanmean(df0<=d2, axis=1)
    Q_net = Q_net.fillna(
        0) + weight*Q0.fillna(0)

Q_net.to_csv(args.o, sep='\t', float_format='%g', header=True)
