import argparse
from string import Template
import os

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='input slurm template file')
parser.add_argument('-o', type=str, help='output slurm sh file', default='gwdg-jobscript-generated.sh')
parser.add_argument('--id1', type=str, default="1", dest='jobid1')
parser.add_argument('--id2', type=str, default="1", dest='jobid2')
parser.add_argument('--Na', type=str, default="100")
parser.add_argument('--No', type=str, default="500")
parser.add_argument('--Ro', type=str, default="5")
parser.add_argument('--phi', type=str, default="0.3", help='volume fraction of obstacles', dest='phi')
parser.add_argument('--t-re', type=str, help="reorientation or tumbling time-scale in the unit of t0 (inverse of the tumbliing rate)", default="1e-2")
parser.add_argument('--BG', type=str, default="1", dest='BG', help='reorientation/tumblling prob. (default: 1)')
parser.add_argument('-G', help='Gmode function: uniform(classic tumblling), sigmoid, norm etc.', type=str, default='uniform', dest='Gmode')
parser.add_argument('--tumble-mode', '--TM', type=str, default='random', help='reorientation type: random (default) or reverse', dest='tumble_mode')
parser.add_argument('-T', type=str, default="1e-2")
parser.add_argument('--alpha', type=str, default="1e2")
parser.add_argument('--Pe', type=str, default="1e2")
parser.add_argument('--tgsd', type=str, default="1e-2", help='dump period in the unit of t0')
parser.add_argument('--lrc', type=str, default="2.5")
parser.add_argument('--dim', type=str, default="2")
parser.add_argument('--dt', type=str, default="1e-4")
parser.add_argument('--rt', type=str, default="1e4")
parser.add_argument('--device', type=str, default="cpu")
parser.add_argument('--phimax', type=str, default="0.91")
parser.add_argument('--phi0', type=str, default="0.63")

args = parser.parse_args()

input_dict = vars(args)
print(input_dict)

with open(args.i, 'r') as f_temp:
#with open(args.i, 'r') as f_temp:
   temp_str = f_temp.read()
temp_obj = Template(temp_str)
out_str = temp_obj.substitute(input_dict)
with open(args.o, 'w') as f_out:
    f_out.write(out_str)
