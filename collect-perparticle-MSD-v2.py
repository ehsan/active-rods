import numpy as np
import pandas as pd
import glob, os, re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--i-flist', type=str, dest='iflist')
parser.add_argument('-o', type=str)
parser.add_argument('--t-re', type=str, dest='t_re')
parser.add_argument('--dt', type=str, dest='dt')
parser.add_argument('--tgsd', type=str, dest='tgsd')
args = parser.parse_args()

ofname = args.o
#flist = glob.glob(tfname.format(phi=phi, Pe=Pe, t_re=t_re, dt=dt, tgsd=tgsd, id='*'))
with open(args.iflist) as iflist:
    flist = iflist.readlines()
    
df = pd.DataFrame()
#re_pattern = r'(?<=lrcut2.5-).*(?=-direct)'
#flist.sort(key=lambda x: re.findall(re_pattern, x)[0])
for idx, f0 in enumerate(flist):
    df0 = pd.read_csv(f0[:-1], delim_whitespace=True, index_col=0)
    if len(df) == 0:
        df['index'] = df0.index
        df.set_index('index', inplace=True)
    df[idx] = df0['MSD']
df['MSD'] = np.nanmean(df.values, axis=1)
df.to_csv(ofname, sep='\t', float_format='%g')
