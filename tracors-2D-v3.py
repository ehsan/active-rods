import numpy as np
import hoomd
import hoomd.md as md
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('--N-t', '--N-tracors', help='Number of tracors', type=int, default=1, dest='N_t')
parser.add_argument('--N-o', '--N-obstacles', help='Number of obstacles', type=int, default=0, dest='N_p')
parser.add_argument('--R-t', help='Radius of tracors', type=float, default=0.5, dest='R_t')
parser.add_argument('--R-o', help='Radius of obstacles', type=float, default=5, dest='R_o')
parser.add_argument('-L', help='Box size', type=float, default=100, dest='L')
parser.add_argument('-T', '--KT', help='Temperature', type=float, default=1.0, dest='T')
parser.add_argument('--gamma', help='Viscous drag coefficient', type=float, default=1.0, dest='gamma')
parser.add_argument('--run-time', '--rt', help='Simulation final time step.', type=float, dest='run_time')
parser.add_argument('--dump-period', help='GSD fump period', type=float, dest='dump_period')
parser.add_argument('--dump-fname', help='GSD dump filename (without [simid].gsd)', type=str, dest='dump_fname', default='dump')
parser.add_argument('--restart-fname', help='GSD dump filename (without [simid].gsd)', type=str, dest='restart_fname', default='restart')
parser.add_argument('--simid', help='Simulation informative id', type=str, default=None, dest='simid')

args = parser.parse_args()
dt = 0.01
particle_types = ['t', 'o']
particles_parmas = {
    't': dict('R'=args.R_t),
    'o': dict('R'=args.R_o)
}
# dump and restart gsd files
fname_temp = '{}.gsd'
if (args.simid != None):
    fname_temp = '{}-{}.gsd'
dump_fname = fname_temp.format(args.dump_fname, args.simid)
restart_fname = fname_temp.format(args.restart_fname, args.simid)
log_fname = fname_temp[:-3].format(args.dump_fname, args.simid) + 'log'
######################################
# Initializing the first snapshot
######################################
snap = gsd.hoomd.Snapshot()
snap.particles.N = args.N_t + args.N_o
snap.particles.types = particle_types
snap.particles.position[:] = np.random.rand(snap.particles.N, 3)*args.L - 0.5*args.L
snap.particles.position[:, 2] = 0 # 2D system
snap.particles.diameter = [args.R_t]*args.N_t + [args.R_o]*args.N_o
snap.particles.typeid = [0]*args.N_t + [1]*args.N_o

#if os.path.exists(restart_fname):
#    snap =

######################################
# Initializing the simulation
######################################
gpu = hoomd.device.GPU()
sim = hoomd.Simulation(device=gpu)

######################################
# Neighbor list and pair potentials 
######################################
# Neighbor list
nl = hoomd.md.nlist.Cell()
# Pair potentials
dpdc = pair.DPDConservative(nlist=nl, r_cut=args.R_t+args.R_o)
for p1, p2 in itertools.combinations_with_replacement(particle_types, 2):
    r_cut = particles_parmas[p1]['R'] + particles_parmas[p2]['R']
    dpdc.params[(p1, p2)] = dict(A=1.0, r_cut=r_cut)

######################################
# Removing overlaps
######################################
fire = md.integrate.mode_minimize(dt=0.001, ftol=1e-6, Etol=1e-7)

######################################
# Brownian dynamics
######################################
# Standard MD integrator
integrator = hoomd.md.Integrator(dt=dt)
integrator.forces.append(dpdc)
brownian = hoomd.md.methods.Brownian(filter=hoomd.filter.Type(particle_types[:1]), kT=args.T, seed=np.random.randint(1000000)+1)
brownian.gamma.default = args.gamma
integrator.methods.append(brownian)

