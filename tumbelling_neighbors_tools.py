import numpy as np
from scipy.stats import norm
from scipy.spatial.transform import Rotation as R
import freud

def intersection_A(r1, r2, d, dim=2):
    if d>=(r1+r2):
        return 0
    if d<=(r1-r2):
        if dim==2:
            return np.pi*r2**2
        else:
            return (4./3.)*np.pi*r2**3
            
    if dim==2:
        d1 = (r1**2 - r2**2 + d**2)/(2*d)
        d2 = d - d1
        res1 = r1**2 * np.arccos(d1/r1) - d1*np.sqrt(r1**2 - d1**2)
        res2 = r2**2 * np.arccos(d2/r2) - d2*np.sqrt(r2**2 - d2**2)
        return res1 + res2
    else:
        res1 = np.pi/(12*d)
        res2 = (r1 + r2 - d)**2
        res3 = (d**2 + 2*d*(r1 + r2) - 3*(r1 - r2)**2)
        return res1*res2*res3

class Tumbller:
    """ G_mode can be set to either uniform or exp"""
    def __init__(self, system=None, box=None, local_r_cut=1.0, r_max=1.0, agent_type='trimer', G_mode='linear', alpha=2, phi_max=0.9069, log=False, log_dict=None, phi0=None, BG=0.0, tumble_mode='uniform', seed=None):
        self.rng = np.random.default_rng(seed)
        self.system = system
        self.box = freud.box.Box.from_box(box)
        self.local_r_cut = local_r_cut
        self.r_max = r_max
        self.agent_type = agent_type
        self.tumble_mode = tumble_mode
        self.G_mode = G_mode
        self.alpha = alpha
        self.phi_max = phi_max
        self.phi0 = phi0
        self.BG = BG
        self.delta_local_quant = 1.0
        self.min_local_quant = 0.0
        if G_mode=='exp':
            self.C = alpha/(np.exp(alpha*phi_max) - 1)
            #print('alpha, C:', alpha, self.C)
            self.delta_local_quant = (self.exp_phi(self.phi_max) - self.exp_phi(0))
            self.min_local_quant = self.exp_phi(0)
        elif G_mode=='sigmoid':
            self.C = alpha * (np.log(1+np.exp(alpha*(phi_max-phi0)))-np.log(1+np.exp(alpha*(0-phi0))))**-1
            self.delta_local_quant = (self.sigmoid_phi(self.phi_max) - self.sigmoid_phi(0))
            self.min_local_quant = self.sigmoid_phi(0)
        elif G_mode=='norm':
            self.C = 1/(1+BG)
            self.delta_local_quant = self.C*(norm.pdf(phi0, loc=phi0, scale=alpha) - norm.pdf(0, loc=phi0, scale=alpha)) # max happenst at phi0
            self.min_local_quant = self.C*(BG + norm.pdf(0, loc=phi0, scale=alpha))
        elif G_mode=='uniform':
            self.delta_local_quant = 1.0
            self.min_local_quant = 0.0
            self.C = 1.0
        else:
            self.C = 2/phi_max**2
            self.delta_local_quant = 2.0/phi_max
            self.min_local_quant = 0.0
        self.log = log
        self.log_dict = log_dict

    def exp_phi(self, x):
        Pphi = np.exp(self.alpha*x)*self.C
        return Pphi

    def sigmoid_phi(self, x):
        res = self.C / (1 + np.exp(-self.alpha * (x - self.phi0)))
        return res

    def local_measure_z(self, nlist, N1=None):
        z = np.zeros(N1)
        for ii, (i, j) in enumerate(nlist):
            z[i] += 1
        return 1*(z/4.0)

    def local_phi(self, nlist, N1=None, snap=None, obstacle_particle_ids=None):
        phi_list = np.zeros(N1)
        if snap.box.dimensions == 2:
            local_A = np.pi*self.local_r_cut**2
        else:
            local_A = (4./3.)*np.pi*self.local_r_cut**3
        for ii, (i, j) in enumerate(nlist):
            r2 = 0.5 * snap.particles.diameter[obstacle_particle_ids][j]
            phi0 = intersection_A(r1=r2, r2=self.local_r_cut, d=nlist.distances[ii], dim=snap.box.dimensions)
            phi_list[i] += phi0
        return phi_list/local_A

    def callback1(self, timestep):
        snap = self.system.take_snapshot(all=True)
        active_particle_ids = snap.particles.typeid == snap.particles.types.index('Active')
        obstacle_particle_ids = snap.particles.typeid == snap.particles.types.index('Obstacle')
        head_particle_ids = snap.particles.typeid == snap.particles.types.index('Head')
        Na = np.sum(active_particle_ids)
        active_pos = snap.particles.position[active_particle_ids]
        obstacle_pos = snap.particles.position[obstacle_particle_ids]
        head_pos = snap.particles.position[head_particle_ids]

        # generate random numbers to check with BG
        if self.G_mode=='uniform':
            random_num = np.zeros(Na)
        else:
            random_num = self.rng.random(size=Na)
            
        local_quant0 = np.zeros(Na)
        if (len(obstacle_pos) > 0):
            aq = freud.locality.AABBQuery(self.box, obstacle_pos)
            if 'trimer' in self.agent_type:
                query_result = aq.query(head_pos, dict(r_max=self.r_max, exclude_ii=False))
            else:
                query_result = aq.query(active_pos, dict(r_max=self.r_max, exclude_ii=False))
            nlist = query_result.toNeighborList()
            # local_quant = self.local_measure_z(nlist, N1=len(active_pos))
            local_quant0 = self.local_phi(nlist, N1=Na, snap=snap, obstacle_particle_ids=obstacle_particle_ids)
            if self.G_mode=='exp':
                local_quant = self.exp_phi(local_quant0)
            elif self.G_mode=='sigmoid':
                local_quant = self.sigmoid_phi(local_quant0)
            elif self.G_mode=='norm':
                local_quant = self.C*(self.BG + norm.pdf(local_quant0, loc=self.phi0, scale=self.alpha))
            else:
                local_quant = self.C*local_quant0
        if self.G_mode=='uniform':
            local_quant = np.ones(Na)*self.BG

        # set new orientations
        old_orientation = snap.particles.orientation[active_particle_ids]
        oriquat = R.from_quat(old_orientation)
        change_ori_id = np.logical_and((np.random.rand(Na)*self.delta_local_quant + self.min_local_quant) < local_quant, random_num < self.BG)
        
        #print(local_quant0, local_quant, change_ori_id)
        random_angles = self.rng.random((Na, 3))
        random_angles[:, 0] *= 2*np.pi
        random_angles[:, 1] *= np.pi/2 - np.pi/4.
        random_angles[:, 2] = 0.
        if (self.tumble_mode == 'reverse'):
            random_angles[:, :] = np.pi
        #print('dim:', snap.box.dimensions)
        if (snap.box.dimensions == 2)or(self.tumble_mode == 'reverse'):
            random_angles[:, 1:] = 0
        # print('rand angle:', random_angles)
        oriquat1 = R.from_rotvec(random_angles)
        oriquat2 = oriquat1 * oriquat
        new_orientation = oriquat2.as_quat()
        #cos_v = oriquat2.as_quat()[:, 0]
        #sin_v = oriquat2.as_quat()[:, 1:]
        #print('aaa')
        #snap.particles.orientation[active_particle_ids, 0] = np.logical_not(random_dice)*old_orientation[:, 0] + cos_v * random_dice
        #snap.particles.orientation[active_particle_ids, 1:] = np.logical_not(random_dice_v)*old_orientation[:, 1:] + sin_v*u*random_dice_v
        # print(old_orientation, oriquat.as_rotvec())
        # print(new_orientation, oriquat2.as_rotvec())
        change_ori2 = np.repeat(change_ori_id.reshape(Na, 1), 4, axis=1)
        snap.particles.orientation[active_particle_ids] = np.logical_not(change_ori2)*old_orientation[:] + change_ori2*new_orientation
        # print(snap.particles.orientation[active_particle_ids])
#        else:
#            if self.tumble_mode=='uniform':
#                cos_v = np.cos(random_theta*0.5)
#                sin_v = np.repeat(np.sin(random_theta.reshape(Na, 1)*0.5), 3, axis=1)
#                snap.particles.orientation[active_particle_ids, 0] = np.logical_not(random_dice)*old_orientation[:, 0] + cos_v * random_dice
#                snap.particles.orientation[active_particle_ids, 1:] = np.logical_not(random_dice_v)*old_orientation[:, 1:] + sin_v*u*random_dice_v
#            else:
#                #oriquat = R.from_quat(old_orientation[:, [3, 0, 1, 2]])
#                oriquat = R.from_quat(old_orientation)
#                #oriquat2 = R.from_euler('zyx', oriquat.as_euler('zyx', degrees=True), degrees=True)
#                oriquat1 = R.from_rotvec([np.pi, 0, 0])
#                oriquat2 = oriquat1 * oriquat
#
#                cos_v = oriquat2.as_quat()[:, 0]
#                sin_v = oriquat2.as_quat()[:, 1:]
#                #print('aaa')
#                snap.particles.orientation[active_particle_ids, 0] = np.logical_not(random_dice)*old_orientation[:, 0] + cos_v * random_dice
#                snap.particles.orientation[active_particle_ids, 1:] = np.logical_not(random_dice_v)*old_orientation[:, 1:] + sin_v*u*random_dice_v
            
        #if snap.box.dimensions==2:
            #if (snap.particles.orientation[active_particle_ids, 1:]>0).any():
                #print('Booooosg')
                #print(u)
        snap.particles.charge[active_particle_ids] = local_quant0
        snap.particles.mass[active_particle_ids] = local_quant
        if np.sum(head_particle_ids)>0:
            #snap.particles.charge[tail_particle_ids] = local_quant0
            snap.particles.charge[head_particle_ids] = local_quant0
            #snap.particles.mass[tail_particle_ids] = local_quant
            snap.particles.mass[head_particle_ids] = local_quant
        if (self.log) and ((np.count_nonzero(change_ori_id))>0):
            for k in np.nonzero(change_ori_id)[0]:
                #print(random_dice, np.nonzero(random_dice), k, self.log_dict, timestep)
                self.log_dict[k].append(timestep)
        # restore the snapshot
        self.system.restore_snapshot(snap)
    
